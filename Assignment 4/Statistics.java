import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.Map;
import java.util.TreeMap;
import java.util.LinkedList;

public class Statistics {
    private static String readString(BufferedReader buffer) throws IOException {
        return buffer.readLine().trim();
    }
    public static void main(String[] args) throws IOException {
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader buffer = new BufferedReader(input);
        int listEntryCount = Integer.parseInt(readString(buffer));
        TreeMap<String, LinkedList<String>> statisticMap = new TreeMap<>();
        while (listEntryCount > 0) {
            String[] listEntries = readString(buffer).split("\\s+");
            String person = listEntries[0];
            String favoriteGame = listEntries[1];
            LinkedList<String> list;
            if (statisticMap.containsKey(favoriteGame)) {
                list = statisticMap.get(favoriteGame);
                if (!list.contains(person)) {
                    list.add(person);
                    statisticMap.put(favoriteGame, list);
                }
            } else {
                list = new LinkedList<>();
                list.add(person);
                statisticMap.put(favoriteGame, list);
            }
            listEntryCount--;
        }
        int max = 0;
        StringBuffer tempKey = new StringBuffer();
        for (Map.Entry<String, LinkedList<String>> statisticsMapEntry : statisticMap.entrySet()) {
            if(statisticsMapEntry.getValue().size() > max && (statisticsMapEntry.getKey().compareTo(tempKey.toString())) > 0){
                max  = statisticsMapEntry.getValue().size();
                tempKey = new StringBuffer(statisticsMapEntry.getKey());
            }
        }
        System.out.println(tempKey);
        if (statisticMap.get("football") == null) System.out.println(0);
        else System.out.println(statisticMap.get("football").size());
        System.out.flush();
        buffer.close();
        input.close();
    }
}
