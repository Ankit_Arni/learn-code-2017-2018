import java.util.*;

class FriendsOperations {
	static Scanner scanner = new Scanner(System.in);
	static int testCases, numberOfFriends = 0, numberOfFriendsToDelete = 0, currentPosition;

	static int readNumber() {
		return Integer.parseInt(scanner.next());
	}

	static boolean comparePopularityBetweenFriends(ArrayList<Integer> friendsList) {
		return currentPosition != 0 && currentPosition > 0 && friendsList.get(currentPosition) > friendsList.get(currentPosition - 1);
	}

	static void updateFriendsToDelete() {
		numberOfFriendsToDelete--;
		currentPosition--;
	}
}

class RemoveFriends extends FriendsOperations {
	public static void main(String args[]) {
		testCases = readNumber();

		int test = 0;
		while (test < testCases) {
			numberOfFriends = readNumber();
			numberOfFriendsToDelete = readNumber();
			ArrayList<Integer> friendsList = new ArrayList<>();

			int iterator = 0;
			currentPosition = 0;

			while (iterator < numberOfFriends) {
				friendsList.add(readNumber());
				if(numberOfFriendsToDelete > 0 && comparePopularityBetweenFriends(friendsList)) {
					friendsList.remove(currentPosition-1);
					updateFriendsToDelete();
				}
				else currentPosition++;
				iterator++;
			}

			while (numberOfFriendsToDelete > 0 && currentPosition < friendsList.size()) {
				if (comparePopularityBetweenFriends(friendsList)) {
					friendsList.remove(currentPosition-1);
					updateFriendsToDelete();
				}
				else currentPosition++;
			}

			for (Integer friend : friendsList)
				System.out.print(friend + " ");
			System.out.println();

			friendsList.clear();
			test++;
		}
		scanner.close();
	}
}