#!/usr/bin/env python3

import sys
from collections import deque
from itertools import islice

spiders_queue = deque()
max_spider_power = 0

def main():
	number_of_initial_spiders, number_of_dequeued_spiders = input_first_line()
	spiders_queue = deque()
	spiders_queue = input_second_line(number_of_dequeued_spiders, spiders_queue)
	for _ in range(number_of_dequeued_spiders):	
		check_all_spider_powers_equals_zero(spiders_queue)
		max_spider_index = get_max_spider_index(spiders_queue, number_of_dequeued_spiders)
		spiders_batch_dequeue_enqueue(spiders_queue, number_of_dequeued_spiders, max_spider_index)

def input_first_line():
	num_ini_spiders, num_deq_spiders = input_initial_value_pair()
	initial_condition_result = validate_initial_conditions(num_ini_spiders, num_deq_spiders)
	while not(initial_condition_result):
		num_ini_spiders, num_deq_spiders = input_initial_value_pair()
	return num_ini_spiders, num_deq_spiders

def input_initial_value_pair():
	num_ini_spiders, num_deq_spiders = map(int, input().split())
	return num_ini_spiders, num_deq_spiders

def validate_initial_conditions(num_ini_spiders, num_deq_spiders):
	if not(1 <= num_deq_spiders <= 316 or num_deq_spiders <= num_ini_spiders <= num_deq_spiders**2):
		return False
	return True

def input_second_line(num_deq_spiders, queue):
	spiders_power_list = input_spider_powers()
	power_condition_result = validate_spider_power_conditions(spiders_power_list, num_deq_spiders)
	while not(power_condition_result):
		spiders_power_list = input_spider_powers()
	queue = add_spiders_to_queue(spiders_power_list, queue)
	return queue

def input_spider_powers():
	power_list = [int(power) for power in input().split()]
	return power_list

def validate_spider_power_conditions(power_list, num_deq_spiders):
	for spider_power in power_list:
		if not(1 <= spider_power <= num_deq_spiders):
			return False
	return True

def add_spiders_to_queue(power_list, queue):
	spider_index = 1
	for spider_power in power_list:
		queue.append([spider_power, spider_index])
		spider_index += 1
	return queue

def check_all_spider_powers_equals_zero(spiders_queue):
	if all(item[0] == 0 for item in spiders_queue):
		temp_spider = spiders_queue.popleft()
		print(temp_spider[1], '', end = '')

def get_max_spider_index(spiders_queue, num_deq_spiders):
	count = 0
	max_spider_power = 0; max_spider_index = 0
	for spider in spiders_queue:
		if (count < num_deq_spiders):
			count += 1 
			spider_power = spider[0]
			spider_index = spider[1]
			if spider_power > max_spider_power:
				max_spider_power = spider_power
				max_spider_index = spider_index
	return max_spider_index

def spiders_batch_dequeue_enqueue(spiders_queue, num_deq_spiders, max_spider_index):
	if num_deq_spiders > len(spiders_queue):
		num_deq_spiders = len(spiders_queue)
	for _ in range(num_deq_spiders):
		if spiders_queue:		
			temp_spider = spiders_queue.popleft()
			if temp_spider[1] == max_spider_index:
				print(max_spider_index, '', end = '')
			else:
				if temp_spider[0] > 0:
					temp_spider[0] -= 1
				spiders_queue.append(temp_spider)

if __name__ == '__main__':
	main()