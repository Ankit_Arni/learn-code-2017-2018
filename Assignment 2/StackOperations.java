public class StackOperations
{
    static int numberOfTests, numberOfPasses, footballHolder, nextPlayerID;
    static char passType;
    protected static final int minLimit = 0, maxTests = 100, maxPasses = 100000, maxPlayerID = 1000000;
    protected static final char passForward = 'P', passBackward = 'B';

    private static int[] PlayerIDs;

    public StackOperations (int numberOfPasses) // constructor to initialize this class members
    {
        this.numberOfPasses = numberOfPasses;
        PlayerIDs = new int[numberOfPasses];
        footballHolder = 0;
    }

    public void push (int nextPlayerID) // function to push next player to stack
    {
        PlayerIDs[++footballHolder] = nextPlayerID;
    }

    public int pop () // function to pop last player from stack
    {
        return PlayerIDs[footballHolder--];
    }

    public static int peek () // function to get player who possesses the football
    {
        return PlayerIDs[footballHolder];
    }
}
