import java.io.IOException;

public class FootballFest extends ReadOperations
{
    private static final int minLimit = 0, maxTests = 100, maxPasses = 100000, maxPlayerID = 1000000;

    public static boolean validLimit(int value, int min, int max) // function to check if value is within given range
    {
        return (value > min && value <= max);
    }

    public static void main(String[] args) throws IOException
    {
        FootballFest footballFest = new FootballFest();

        footballFest.readInputLine(); // read first line of input

        while(!validLimit(numberOfTests, minLimit, maxTests)) // while not within range, keep reading first line of input
            footballFest.readInputLine();

        testControl:
        while(validLimit(numberOfTests, minLimit, maxTests)) // while within range of tests
        {
            numberOfTests--; // iterator for tests
            footballFest.readInputLine(); // read second line of input

            if (!validLimit(numberOfPasses, minLimit, maxPasses)  || !validLimit(passedTo, minLimit, maxPlayerID))
            // if not within range for either, repeat this iteration of tests loop
            {
                numberOfTests++;
                continue testControl;
            }

            passControl:
            while(validLimit(numberOfPasses, minLimit, maxPasses)) // while within range of passes
            {
                numberOfPasses--; // iterator for passes
                footballFest.readInputLine(); // read subsequent lines of input

                if (!validLimit(passedTo, minLimit, maxPlayerID) || (passType != passForward && passType != passBackward))
                // if not within range, or if type of pass is invalid, repeat this iteration of passes loop
                {
                    numberOfPasses++;
                    continue passControl;
                }
            }

            System.out.println("Player " + passedTo);
        }

        buffer.close();
        stream.close();
    }
}