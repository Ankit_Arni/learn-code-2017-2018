import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ReadOperations
{
    static InputStreamReader stream = new InputStreamReader(System.in);
    static BufferedReader buffer = new BufferedReader(stream);

    static int numberOfTests, numberOfPasses, passedBy = -1, passedTo = -1;

    static String inputLine;
    static char passType;

    static final char passForward = 'P', passBackward = 'B';

    public static void swap(int passedBy, int passedTo) // function to swap the values passed as parameters
    {
        int temp = passedTo;
        passedTo = passedBy;
        passedBy = temp;
    }

    public void readIfDigitInput() throws IOException // function to parse input line if first character is a digit
    {
        if (!inputLine.contains(" ")) // corresponds to first line of input, for number of tests
            numberOfTests = Integer.parseInt(inputLine);

        else // corresponds to second line of input, for number of passes & initial player
        {
            numberOfPasses = Integer.parseInt(inputLine.substring(0, inputLine.indexOf(" ")));
            passedTo = Integer.parseInt(inputLine.substring(inputLine.indexOf(" ")+1));
        }
    }

    public void readIfCharInput() throws IOException // function to parse input line if first character is a letter
    {
        passType = inputLine.charAt(0); // corresponds to subsequent lines of input if passes are within range

        if (passType == passForward) // if type of pass is a forward pass to new player
        {
            passedBy = passedTo;
            passedTo = Integer.parseInt(inputLine.substring(inputLine.indexOf(" ")+1));
        }

        else if (passType == passBackward) // if type of pass is a backward pass to previous player
            swap (passedBy, passedTo);        
    }

    public void readInputLine() throws IOException // function to read the input line as a String
    {
        inputLine = buffer.readLine();

        if (Character.isDigit(inputLine.charAt(0))) // checking if first character of line is a digit
            readIfDigitInput();

        else if (Character.isLetter(inputLine.charAt(0))) // checking if first character of line is a letter
            readIfCharInput();
    }
}