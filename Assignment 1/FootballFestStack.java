import java.util.Scanner;

public class FootballFestStack extends StackOperations
{
    private FootballFestStack (int numberOfPasses) // constructor to initialize base class members
    {
        super (numberOfPasses);
    }

    public static boolean validLimit (int value, int min, int max) // function to check if value is within given range 
    {
        return (value > min && value <= max);
    }

    public static void main (String[] args)
    {
        Scanner input = new Scanner (System.in);

        numberOfTests = input.nextInt (); // scan first integer entered

        while (!validLimit (numberOfTests, minLimit, maxTests)) // while not within range, keep scanning first integer entered
            numberOfTests = input.nextInt ();

        testControl:
        while (validLimit (numberOfTests, minLimit, maxTests)) // while within range of tests
        {
            numberOfTests--; // iterator for tests

            numberOfPasses = input.nextInt (); // scan second integer entered
            nextPlayerID = input.nextInt (); // scan third integer entered

            if (!validLimit (numberOfPasses, minLimit, maxPasses)  || !validLimit (nextPlayerID, minLimit, maxPlayerID))
            // if not within range for either, repeat this iteration for tests loop
            {
                numberOfTests++;
                continue testControl;
            }

            FootballFestStack player = new FootballFestStack (numberOfPasses); // initialize class instance

            player.push (nextPlayerID); // since valid player, push to stack

            passControl:
            while (validLimit (numberOfPasses, minLimit, maxPasses)) // while within range of passes
            {
                numberOfPasses--; // iterotor for passes

                passType = input.next ().charAt (0); // scan first chracter entered

                if (passType == passForward) // if type of pass is a forward pass to new player
                {
                    nextPlayerID = input.nextInt (); // scan subsequent integer entered

                    if (!validLimit (nextPlayerID, minLimit, maxPlayerID))
                    // if not within range, repeat this iteration of passes loop
                    {
                        numberOfPasses++;
                        continue passControl;
                    }

                    player.push (nextPlayerID); // since valid player, push to stack
                }

                else if (passType == passBackward) // if type of pass is a backward pass to previous player
                {
                    int temp1 = player.pop ();
                    int temp2 = player.pop ();
                    player.push (temp2);
                    player.push (temp1);
                }

                else // if type of pass is invalid, repeat this iteration of passes loop
                {
                    numberOfPasses++;
                    continue passControl;
                }
            }

            System.out.println ("Player " + peek ());
        }

        input.close ();
    }
}